public async Task RunAsync()
{
    INgrokManager ngrok;

    if (!Directory.Exists("data/server"))
    {
        Logger.Log(LogType.Info, "Unpacking the server instance, please wait...");

        if (!File.Exists("data/server.zip"))
        {
            Logger.Log(LogType.Error, "Server instance can't be found and is needed !");
            Environment.Exit(1);
            return;
        }

        ZipFile.ExtractToDirectory("data/server.zip", "data/server");

        Logger.Log(LogType.Info, "Server unpacked");
    }
    
    foreach (Process ngrokProcess in Process.GetProcessesByName("ngrok"))
        ngrokProcess.Kill();

    Logger.Log(LogType.Info, "Downloading ngrok...");

    ngrok = new NgrokManager();

    if (OperatingSystem.IsLinux())
    {
        try
        {
            await ngrok.RegisterAuthTokenAsync(File.ReadAllText("ngrok-token.txt"));
        }
        catch (Exception e)
        {
            Logger.Log(LogType.Warning,
                $"Registering ngrok auth token threw an exception ({e.GetType().FullName}), skipping");
        }
    }

    try
    {
        await ngrok.DownloadAndUnzipNgrokAsync();
    }
    catch (Exception e)
    {
        Logger.Log(LogType.Warning, $"Downloading ngrok threw an exception ({e.GetType().FullName}), skipping");
    }

    Logger.Log(LogType.Info, "Starting ngrok...");

    ngrok.StartNgrok(NgrokManager.Region.Europe);

    var tunnel = new StartTunnelDTO
    {
        name = "mcproxy",
        proto = "tcp",
        addr = "25565"
    };

    await Task.Delay(1000);

    Logger.Log(LogType.Info, "Creating ngrok tunnel...");
    bool success = false;
    HttpResponseMessage? resp = null;

    for (int i = 0; i < 5; i++)
    {
        resp = await ngrok.StartTunnelAsync(tunnel);

        if ((int) resp.StatusCode != 201)
        {
            Logger.Log(LogType.Error, $"Failed to create tunnel : {await resp.Content.ReadAsStringAsync()}");
            Logger.Log(LogType.Info, $"Retrying in 1s ({i + 1}/5)");

            await Task.Delay(1000);
            continue;
        }

        success = true;
        break;
    }

    if (!success)
    {
        Logger.Log(LogType.Error, "Cannot create ngrok tunnel, please retry");
        Environment.Exit(1);

        return;
    }

    string json = await resp.Content.ReadAsStringAsync();
    JsonData resultJson = JsonMapper.ToObject(json);

    string publicUrl = ((string) resultJson["public_url"])
        .Replace("tcp://", "").Trim('/');

    Logger.Log(LogType.Info, $"Tunnel created and pointing to '{publicUrl}'");

    Logger.Log(LogType.Info, "Starting Minecraft...");

    Process? java = Process.Start(new ProcessStartInfo
    {
        FileName = "java",
        Arguments = "-Xmx2G -jar fabric-server-mc.1.19.4-loader.0.14.19-launcher.0.11.2.jar nogui",
        WorkingDirectory = "data/server",
        RedirectStandardOutput = true,
        RedirectStandardError = true,
        RedirectStandardInput = true,
        UseShellExecute = false
    });

    JavaProcess = java!;

    var minecraftVerRegex = Regexes.MinecraftVersion(); // (Loading Minecraft).+
    Regex versionRegex = Regexes.SemanticVersionRegex(); // (\\d|\\.)+
    Regex modloaderRegex = Regexes.ModLoaderRegex(); // (with ).+
    Regex playerConnectRegex = Regexes.PlayerConnectRegex(); // (?!: )\\w+ joined the game
    Regex playerDisconnectRegex = Regexes.PlayerDisconnectRegex(); // (?!: )\\w+ left the game
    Regex eventRegex = Regexes.EventRegex(); // (?!(: ))EVENT.+
    ChatMessagesParser chatParser = new();
    CommandRunner cmd = new(java.StandardInput);
    UuidStorage uuids = new();

    Commands = cmd;
    Uuids = uuids;

    while (!java.HasExited)
    {
        string? outputLine = await java.StandardOutput.ReadLineAsync();
        if (outputLine == null) break;

        Logger.Log(LogType.Minecraft, outputLine);

        if (minecraftVerRegex.IsMatch(outputLine))
        {
            MinecraftVersion = versionRegex.Match(minecraftVerRegex.Match(outputLine).Value.Trim()).Value.Trim();
            ModLoader = modloaderRegex.Match(outputLine).Value.Replace("with ", "").Trim();
            
            continue;
        }

        if (playerConnectRegex.IsMatch(outputLine))
        {
            string username = playerConnectRegex.Match(outputLine).Value.Replace(" joined the game", "").Trim();

            Players.Add(new ServerPlayer
            {
                JoinedAt = DateTime.Now,
                Username = username,
                Uuid = uuids.GetUuid(username)
            });

            foreach (ServerFeature feature in Features)
            {
                if (await feature.OnPlayerConnect(username)) break;
            }

            await Craborganizer.Instance.SetStatusAsync(uuids.Count, MinecraftVersion!);
            
            continue;
        }

        if (playerDisconnectRegex.IsMatch(outputLine))
        {
            string username = playerDisconnectRegex.Match(outputLine).Value.Replace(" left the game", "").Trim();
            string uuid = uuids.GetUuid(username);
            uuids.Remove(username);

            foreach (ServerFeature feature in Features)
            {
                if (await feature.OnPlayerDisconnect(username, uuid)) break;
            }

            Players = Players
                .Where(p => p.Uuid != uuid)
                .ToList();

            await Craborganizer.Instance.SetStatusAsync(uuids.Count, MinecraftVersion!);
            
            continue;
        }

        if (uuids.IsUuidLine(outputLine))
        {
            (string username, string uuid)? v = uuids.Process(outputLine);

            if (v.HasValue)
            {
                foreach (ServerFeature feature in Features)
                {
                    if (await feature.OnPlayerUuid(v.Value.uuid)) break;
                }
            }
            
            continue;
        }

        if (chatParser.IsChatMessage(outputLine))
        {
            string avatarUrl = null;
            ChatMessage chatMessage = chatParser.Parse(outputLine)!
                .RemoveForbiddenCharacters();

            foreach (ServerFeature feature in Features)
            {
                if (await feature.OnChatMessage(chatMessage)) break;
            }
            
            continue;
        }

        if (outputLine.Contains("Done") && outputLine.Contains("For help, type \"help\""))
        {
            // The server started successfully

            IP = publicUrl;

            await Craborganizer.Instance.SetStartedAsync(true);
            await Craborganizer.Instance.SetStatusAsync(0, MinecraftVersion);
            
            await Multicrabe.RegisterServerAsync(publicUrl, "Le serveur du crabe", 0, 10);

            foreach (ServerFeature feature in Features)
            {
                if (await feature.OnServerStarted(publicUrl)) break;
            }
            
            continue;
        }

        if (eventRegex.IsMatch(outputLine))
        {
            string eventLine = eventRegex.Match(outputLine).Value;
            CServEvent evt = new CServEvent(eventLine);

            foreach (ServerFeature feature in Features)
            {
                if (await feature.OnEvent(evt)) break;
            }
            continue;
        }
    }

    Logger.Log(java.ExitCode != 0 ? LogType.Error : LogType.Info, $"Minecraft exited with code {java.ExitCode}");

    foreach (ServerFeature feature in Features)
    {
        if (await feature.OnServerExited(java)) break;
    }

    await Multicrabe.UnregisterServerAsync();

    Logger.Log(LogType.Info, "Stopping tunnel...");
    await ngrok.StopTunnelAsync("mcproxy");

    Logger.Log(LogType.Info, "Stopping ngrok...");
    ngrok.StopNgrok();

    Logger.Log(LogType.Info, "Server closed successfully, disconnecting bot...");

    await Craborganizer.Instance.DisconnectAsync();
    
    Environment.Exit(0);
}