# Multicrabe
![logo](front/res/logo_wide_transparent.png)

A central server solution for our (or your) multiplayer games.

It *will* feature :
+ A public server list (accessible via an API)
+ A standard authentication system
+ A way to publish hosted game sessions to be added to the servers list
+ A web interface to view all servers
+ A public ranking board, available from the web interface
+ A matchmaking service with deployable relays

## Project structure

+ `back` : Back-end C# central server
+ `front` : Front-end dashboard website

## Dependencies

+ **[kcp2k](https://github.com/vis2k/kcp2k)** used as a protocol for the relay system with [Mirror](https://github.com/MirrorNetworking/Mirror) (the network library used by our games)