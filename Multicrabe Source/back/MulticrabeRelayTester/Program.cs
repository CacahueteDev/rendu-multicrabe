﻿using kcp2k;
using Multicrabe;
using Multicrabe.Relay.Packets;

Console.Write("Port : ");
string port = Console.ReadLine()!;

ushort parsedPort = ushort.Parse(port);

Console.WriteLine("Connecting...");

KcpClient client = new KcpClient(OnConnected, OnData, OnDisconnected, OnError);
client.Connect("localhost", parsedPort, new KcpConfig());

while (true)
{
    client.Tick();
}

Console.WriteLine("Closing");

void OnConnected()
{
    Console.WriteLine("Connected !");
}

void OnDisconnected()
{
    Console.WriteLine("Disconnected !");
}

void OnData(ArraySegment<byte> data, KcpChannel channel)
{
    RelayPacket packet = new RelayPacket()
        .Deserialize(data.ToArray());

    Console.WriteLine(
        $"Data ! {channel} {packet.Target} {packet.TargetId} {packet.DataTypeHash} {SerializablePacket.RetrieveFromHash(packet.DataTypeHash)!.GetType().FullName}");
}

void OnError(ErrorCode code, string message)
{
    Console.WriteLine($"Error ! {code} {message}");
}