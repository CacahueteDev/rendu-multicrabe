﻿using System.Text.Json;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Multicrabe.Core;
using Multicrabe.Errors;
using Multicrabe.Models;
using FileIO = System.IO.File;

namespace Multicrabe.Controllers;

[Route("/games")]
public class GamesController : ControllerBase
{
    [HttpGet]
    public object Get()
    {
        return Ok(new NoError<string[]>(MulticrabeManager.Games));
    }
}