﻿using System.Text.Json;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Multicrabe.Errors;
using Multicrabe.Models;
using FileIO = System.IO.File;

namespace Multicrabe.Controllers;

[Route("/info")]
public class InfoController : ControllerBase
{
    [HttpGet]
    public object Get()
    {
        if (!FileIO.Exists("server.json")) return NotFound(Responses.ErrorMissingServerInfo);

        return Ok(new NoError<MulticrabeServerInfo>(
            JsonSerializer.Deserialize<MulticrabeServerInfo>(FileIO.ReadAllText("server.json"))!));
    }
}