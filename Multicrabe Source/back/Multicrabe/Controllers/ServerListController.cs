using System.Net;
using Microsoft.AspNetCore.Mvc;
using Multicrabe.Core;
using Multicrabe.Errors;
using Multicrabe.Models;

namespace Multicrabe.Controllers;

[Route("/server")]
public class ServerListController : ControllerBase
{
    [HttpGet("/serverlist/{gameName}")]
    public object Read(string gameName, string? gameVersion)
    {
        if (!MulticrabeConfiguration.ServerListEnabled)
            return Unauthorized(Responses.ErrorServiceDisabled);
        
        if (!MulticrabeManager.ServerList.ContainsKey(gameName))
            return NotFound(Responses.ErrorGameNotRegistered);

        ServerEntry[] array = MulticrabeManager.ServerList[gameName].ToArray();
        if (gameVersion != null) 
            array = array.Where(s => s.GameVersion == gameVersion).ToArray();
        
        return Ok(new NoError<ServerEntry[]>(array));
    }
    
    [HttpPost("/server/{gameName}")]
    public object Create(string gameName, [FromBody] ServerCreationInput input)
    {
        if (!MulticrabeConfiguration.ServerListEnabled)
            return Unauthorized(Responses.ErrorServiceDisabled);
        
        if (!input.Verify()) return BadRequest(Responses.ErrorInvalidInput);
        
        IPAddress clientAddress = Request.HttpContext.Connection.RemoteIpAddress!;
        
        if (!MulticrabeManager.ServerList.ContainsKey(gameName))
            return NotFound(Responses.ErrorGameNotRegistered);
        if (MulticrabeManager.IsAnyServerOwnedByAddress(gameName, clientAddress))
            return BadRequest(Responses.ErrorHostHasAlreadyOneServer);
        
        ServerEntry server = new ServerEntry(gameName, clientAddress, input);
        MulticrabeManager.ServerList[gameName].Add(server);
        
        return Ok(Responses.Success);
    }
    
    [HttpPost("/server/{gameName}/playerCount")]
    public object UpdatePlayerCount(string gameName, [FromForm] int value)
    {
        if (!MulticrabeConfiguration.ServerListEnabled)
            return Unauthorized(Responses.ErrorServiceDisabled);
        
        IPAddress clientAddress = Request.HttpContext.Connection.RemoteIpAddress!;
        
        if (!MulticrabeManager.ServerList.ContainsKey(gameName))
            return NotFound(Responses.ErrorGameNotRegistered);
        
        ServerEntry? clientServer = MulticrabeManager.GetServerOwnedByAddress(gameName, clientAddress);
        if (clientServer == null)
            return NotFound(Responses.ErrorHostHasNoServer);

        if (value > clientServer.Players.Max) value = clientServer.Players.Max;
        else if (value < 0) value = 0;
        
        clientServer.Players.Current = value;
        
        return Ok(Responses.Success);
    }
    
    [HttpDelete("/server/{gameName}")]
    public object Delete(string gameName)
    {
        if (!MulticrabeConfiguration.ServerListEnabled)
            return Unauthorized(Responses.ErrorServiceDisabled);
        
        IPAddress clientAddress = Request.HttpContext.Connection.RemoteIpAddress!;
        
        if (!MulticrabeManager.ServerList.ContainsKey(gameName))
            return NotFound(Responses.ErrorGameNotRegistered);

        ServerEntry? clientServer = MulticrabeManager.GetServerOwnedByAddress(gameName, clientAddress);
        if (clientServer == null)
            return NotFound(Responses.ErrorHostHasNoServer);
        
        MulticrabeManager.RemoveServer(gameName, clientServer);
        
        return Ok(Responses.Success);
    }
}