﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using Multicrabe.Core;
using Multicrabe.Errors;
using Multicrabe.Models;
using Multicrabe.Relay;

namespace Multicrabe.Controllers;

[Route("/relay")]
public class RelayController : ControllerBase
{
    [HttpPost("/relay/{gameName}")]
    public object Post(string gameName)
    {
        if (!MulticrabeConfiguration.RelaysEnabled)
            return Unauthorized(Responses.ErrorServiceDisabled);
        
        IPAddress clientAddress = Request.HttpContext.Connection.RemoteIpAddress!;
        
        if (!MulticrabeManager.ServerList.ContainsKey(gameName))
            return NotFound(Responses.ErrorGameNotRegistered);
        if (MulticrabeManager.IsAnyRelayOwnedByAddress(gameName, clientAddress))
            return BadRequest(Responses.ErrorHostHasAlreadyOneRelay);

        GameRelay relay = MulticrabeManager.GenerateRelay(gameName, clientAddress);
        return Ok(new NoError<GameRelay>(relay));
    }
    
    [HttpGet("/relay/{gameName}/{relayId}")]
    public object Get(string gameName, string relayId)
    {
        if (!MulticrabeConfiguration.RelaysEnabled)
            return Unauthorized(Responses.ErrorServiceDisabled);

        if (!MulticrabeManager.ServerList.ContainsKey(gameName))
            return NotFound(Responses.ErrorGameNotRegistered);

        GameRelay? relay = MulticrabeManager.GetRelayById(gameName, relayId);
        
        if (relay == null)
            return NotFound(Responses.ErrorIdNotFound);
        
        return Ok(new NoError<GameRelay>(relay));
    }
}