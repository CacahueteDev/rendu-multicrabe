﻿using System.Text;

namespace Multicrabe;

public static class Utilities
{
    public static ushort GetTypeHash(this Type t) 
        => Crc16Ccitt(Encoding.UTF8.GetBytes(t.Name));
    
    // taken from https://stackoverflow.com/a/34943214
    public static ushort Crc16Ccitt(byte[] bytes)
    {
        const ushort poly = 4129;
        ushort[] table = new ushort[256];
        ushort initialValue = 0xffff;
        ushort temp, a;
        ushort crc = initialValue;
        for (int i = 0; i < table.Length; ++i)
        {
            temp = 0;
            a = (ushort)(i << 8);
            for (int j = 0; j < 8; ++j)
            {
                if (((temp ^ a) & 0x8000) != 0)
                    temp = (ushort)((temp << 1) ^ poly);
                else
                    temp <<= 1;
                a <<= 1;
            }
            table[i] = temp;
        }
        for (int i = 0; i < bytes.Length; ++i)
        {
            crc = (ushort)((crc << 8) ^ table[((crc >> 8) ^ (0xff & bytes[i]))]);
        }
        return crc;
    }
}