﻿namespace Multicrabe.Errors;

public static class Responses
{
    public static readonly NoError<string> Success
        = new("success");
    public static readonly ErrorBase ErrorServiceDisabled
        = new("CR-000", "This service is disabled");
    public static readonly ErrorBase ErrorInvalidInput
        = new("CR-001", "The passed input data is invalid");
    public static readonly ErrorBase ErrorGameNotRegistered
        = new("CR-002", "The passed game is not registered");
    public static readonly ErrorBase ErrorHostHasNoServer
        = new("CR-003", "Your hostname is not associated to any server in the list");
    public static readonly ErrorBase ErrorHostHasAlreadyOneServer
        = new("CR-004", "Your hostname is already associated with an existing server in the list");
    public static readonly ErrorBase ErrorHostHasAlreadyOneRelay
        = new("CR-005", "Your hostname is already associated with an existing opened relay");
    public static readonly ErrorBase ErrorIdNotFound
        = new("CR-006", "No resource associated with the passed identifier");
    public static readonly ErrorBase ErrorMissingServerInfo
        = new("CR-007", "This server does not have associated information");
}