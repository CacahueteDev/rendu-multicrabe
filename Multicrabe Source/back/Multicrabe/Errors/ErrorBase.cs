﻿namespace Multicrabe.Errors;

public class ErrorBase
{
    public bool IsError => true;
    public string Code { get; }
    public string Message { get; }

    public ErrorBase(string code, string message)
    {
        Code = code;
        Message = message;
    }
}

public class NoError<T>
{
    public bool IsError => false;
    public T Data { get; }

    public NoError(T data)
    {
        Data = data;
    }
}