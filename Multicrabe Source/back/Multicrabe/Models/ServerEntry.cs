﻿using System.Net;
using System.Text.Json.Serialization;

namespace Multicrabe.Models;

public class ServerEntry
{
    public string Name { get; set; }
    public string GameName { get; set; }
    public string GameVersion { get; set; }
    public PlayerCount Players { get; set; }
    public string Address { get; set; }
    [JsonIgnore]
    public IPAddress OwnerAddress { get; set; }

    public ServerEntry(string gameName, IPAddress ownerAddress, ServerCreationInput input)
    {
        Name = input.Name;
        GameName = gameName;
        GameVersion = input.GameVersion;
        OwnerAddress = ownerAddress;
        Players = new PlayerCount(1, input.MaxPlayers);
        Address = input.Address;
    }
    
    public ServerEntry(string name, string gameName, string gameVersion, string address, PlayerCount players)
    {
        Name = name;
        GameName = gameName;
        GameVersion = gameVersion;
        Players = players;
        Address = address;
    }
}