﻿using System.Text.Json.Serialization;

namespace Multicrabe.Models;

public class MulticrabeServerInfo
{
    [JsonPropertyName("name")]
    public string Name { get; set; }
    
    [JsonPropertyName("type")]
    public string Type { get; set; }
    
    [JsonPropertyName("id")]
    public string Id { get; set; }
    
    [JsonPropertyName("country")]
    public string Country { get; set; }
}