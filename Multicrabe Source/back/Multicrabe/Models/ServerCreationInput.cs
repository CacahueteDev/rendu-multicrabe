﻿using System.ComponentModel.DataAnnotations;

namespace Multicrabe.Models;

public class ServerCreationInput
{
    [Required]
    public string Name { get; set; }
    [Required]
    public string Address { get; set; }
    [Required]
    public string GameVersion { get; set; }
    [Required]
    public int MaxPlayers { get; set; }

    public bool Verify()
    {
        if (string.IsNullOrWhiteSpace(Name) || Name.Length < 2) return false;
        if (string.IsNullOrWhiteSpace(Address) || Address == "localhost" || Address == "127.0.0.1") return false;
        if (string.IsNullOrWhiteSpace(GameVersion) || GameVersion.Length < 2) return false;
        if (MaxPlayers < 2) return false;

        return true;
    }
}