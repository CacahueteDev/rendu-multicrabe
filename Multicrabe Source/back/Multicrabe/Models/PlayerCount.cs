﻿namespace Multicrabe.Models;

public class PlayerCount
{
    public int Current { get; set; }
    public int Max { get; set; }

    public PlayerCount(int current, int max)
    {
        Current = current;
        Max = max;
    }

    public override string ToString() => $"{Current}/{Max}";
}