﻿namespace Multicrabe.Core;

public static class MulticrabeConfiguration
{
    public const bool ServerListEnabled = true;
    public const bool RelaysEnabled = false;
}