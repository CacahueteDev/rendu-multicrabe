﻿using System.Net;
using Multicrabe.Models;
using Multicrabe.Relay;

namespace Multicrabe.Core;

public static class MulticrabeManager
{
    public static Dictionary<string, List<ServerEntry>> ServerList { get; } = new();
    public static Dictionary<string, List<GameRelay>> Relays { get; } = new();
    public static List<int> OpenedRelayPorts { get; } = new();
    public static string[] Games => ServerList.Select(kv => kv.Key).ToArray();

    public static void Init()
    {
        InitGame("firelabs");
        InitGame("minecraft");
    }

    public static void InitGame(string gameName)
    {
        ServerList.Add(gameName, new List<ServerEntry>());
        Relays.Add(gameName, new List<GameRelay>());
    }

    public static GameRelay GenerateRelay(string gameName, IPAddress host)
    {
        if (!ServerList.ContainsKey(gameName)) InitGame(gameName);
        GameRelay? ownedRelay = GetRelayOwnedByAddress(gameName, host);

        if (ownedRelay != null) return ownedRelay;

        GameRelay relay = new GameRelay(RelayGenerator.GenerateId(), RelayGenerator.GeneratePort(OpenedRelayPorts),
            host);
        Relays[gameName].Add(relay);
        // TODO: Open kcp relay server

        OpenedRelayPorts.Add(relay.Port);

        return relay;
    }

    public static bool CloseRelay(string gameName, string id)
    {
        GameRelay? relay = Relays[gameName].FirstOrDefault(r => r.Id == id);
        if (relay == null) return false;

        relay.Stop();
        
        OpenedRelayPorts.Remove(relay.Port);
        Relays[gameName].Remove(relay);

        return true;
    }

    public static GameRelay? GetRelayById(string gameName, string id)
        => Relays[gameName].FirstOrDefault(relay => relay.Id == id);

    public static void CreateServer(string gameName, ServerEntry server)
    {
        if (!ServerList.ContainsKey(gameName)) InitGame(gameName);

        server.GameName = gameName;
        ServerList[gameName].Add(server);
    }

    public static void RemoveServer(string gameName, ServerEntry server)
        => ServerList[gameName].Remove(server);

    public static ServerEntry? GetServerOwnedByAddress(string gameName, IPAddress address)
        => ServerList[gameName].FirstOrDefault(s => Equals(s.OwnerAddress, address));

    public static GameRelay? GetRelayOwnedByAddress(string gameName, IPAddress address)
        => Relays[gameName].FirstOrDefault(s => Equals(s.Host, address));

    public static bool IsAnyServerOwnedByAddress(string gameName, IPAddress address)
        => ServerList[gameName].FirstOrDefault(s => s.OwnerAddress.Equals(address)) != null;

    public static bool IsAnyRelayOwnedByAddress(string gameName, IPAddress address)
        => Relays[gameName].FirstOrDefault(s => s.Host.Equals(address)) != null;
}