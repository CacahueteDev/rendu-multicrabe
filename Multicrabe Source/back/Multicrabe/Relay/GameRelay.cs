﻿using System.Net;
using System.Text.Json.Serialization;
using kcp2k;
using Multicrabe.Relay.Packets;

namespace Multicrabe.Relay;

public class GameRelay
{
    KcpServer relayServer;
    int hostConnectionId = -1;

    public ushort Port { get; }
    public string Id { get; }
    [JsonIgnore] public IPAddress Host { get; }

    public GameRelay(string id, ushort port, IPAddress host)
    {
        Id = id;
        Port = port;
        Host = host;

        relayServer = new KcpServer(OnConnected, OnData, OnDisconnected, OnError, new KcpConfig());
        relayServer.Start(port);

        Console.WriteLine($"Multicrabe GameRelay (kcp) started on port {port} by {host}");
        
        // TODO: Tick the server !
    }

    public void Stop()
    {
        relayServer.Stop();

        Console.WriteLine($"Multicrabe GameRelay (kcp) stopped on port {Port}");
    }

    public void Tick()
    {
        relayServer.Tick();
    }

    void SendPacket(int connectionId, KcpChannel channel, RelayPacket packet)
    {
        using MemoryStream ms = new MemoryStream();
        using BinaryWriter wr = new BinaryWriter(ms);

        packet.WriteTo(wr);

        relayServer.Send(connectionId, ms.ToArray(), channel);
    }

    void SendPacketToHost(KcpChannel channel, RelayPacket packet)
    {
        if (hostConnectionId == -1) throw new Exception("Host cannot be found on relay");

        SendPacket(hostConnectionId, channel, packet);
    }

    void OnHostData(ArraySegment<byte> data, KcpChannel channel)
    {
        RelayPacket packet = new RelayPacket()
            .Deserialize(data.ToArray());

        if (packet.DataTypeHash == typeof(TransferPacketData).GetTypeHash() && packet.Target == PacketTarget.Client)
        {
            SendPacket(packet.TargetId, channel, packet);
            
            return;
        }

        if (packet.Target != PacketTarget.Relay) return;

        if (packet.DataTypeHash == typeof(DisconnectUserPacketData).GetTypeHash())
        {
            RelayPacket<DisconnectUserPacketData> unpacked = packet.Unpack<DisconnectUserPacketData>();
            if (unpacked.Data.ConnectionId == 0) return;
            
            relayServer.Disconnect(unpacked.Data.ConnectionId);
            
            Console.WriteLine($"Host asked to disconnect connection #{unpacked.Data.ConnectionId}");

            return;
        }
    }

    private void OnConnected(int connectionId)
    {
        IPEndPoint? endPoint = relayServer.GetClientEndPoint(connectionId);
        if (endPoint == null) return;

        if (endPoint.Address.Equals(Host))
        {
            if (hostConnectionId == -1)
            {
                hostConnectionId = connectionId;

                Console.WriteLine($"Host is now connection #{connectionId}");
                return;
            }

            Console.WriteLine("Another connection coming from host, considering it as a standard client");
        }

        SendPacketToHost(KcpChannel.Reliable,
            new RelayPacket<UserEventPacketData>(PacketTarget.Host,
                new UserEventPacketData(connectionId, UserEventPacketData.EventType.Connected)));
    }

    private void OnDisconnected(int connectionId)
    {
        if (hostConnectionId == -1) return;
        
        if (connectionId == hostConnectionId)
        {
            Console.WriteLine("Host disconnected");
            Stop();
            
            return;
        }
        
        SendPacketToHost(KcpChannel.Reliable,
            new RelayPacket<UserEventPacketData>(PacketTarget.Host,
                new UserEventPacketData(connectionId, UserEventPacketData.EventType.Disconnected)));
    }

    private void OnError(int connectionId, ErrorCode code, string message)
    {
    }

    private void OnData(int connectionId, ArraySegment<byte> data, KcpChannel channel)
    {
        if (connectionId == hostConnectionId)
        {
            OnHostData(data, channel);
            return;
        }
        
        RelayPacket packet = new RelayPacket()
            .Deserialize(data.ToArray());

        if (packet.Target != PacketTarget.Host && packet.TargetId != 0) return;

        if (packet.DataTypeHash == typeof(TransferPacketData).GetTypeHash())
        {
            SendPacket(hostConnectionId, channel, packet);
            
            return;
        }
    }
}