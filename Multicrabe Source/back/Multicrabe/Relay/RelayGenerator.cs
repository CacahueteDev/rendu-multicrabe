﻿namespace Multicrabe.Relay;

public static class RelayGenerator
{
    public static readonly Range PortRange = 10000..60000;
    public static readonly string IdGeneratorCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static readonly int IdMaxLength = 6;

    public static ushort GeneratePort(List<int> exclusions)
    {
        int port = 0;

        while (true)
        {
            port = Random.Shared.Next(PortRange.Start.Value, PortRange.End.Value);

            if (!exclusions.Contains(port)) break;
        }

        return (ushort)port;
    }

    public static string GenerateId()
    {
        char[] id = new char[IdMaxLength];

        for (int i = 0; i < IdMaxLength; i++)
            id[i] = IdGeneratorCharacters[Random.Shared.Next(0, IdGeneratorCharacters.Length)];

        return new string(id);
    }
}