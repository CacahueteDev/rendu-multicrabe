﻿namespace Multicrabe.Relay.Packets;

public class RelayPacket : SerializablePacket
{
    public PacketTarget Target { get; set; }
    public int TargetId { get; set; }
    public ushort DataTypeHash { get; set; }
    public byte[] RawData { get; set; }
    public bool IsInvalid { get; private set; }
    
    public override void ReadFrom(BinaryReader rd)
    {
        string magic = new string(rd.ReadChars(4));

        if (magic != "CRRL")
        {
            Console.WriteLine("Received invalid GameRelay packet");
            IsInvalid = true;
            return;
        }

        Target = (PacketTarget)rd.ReadByte();
        TargetId = rd.ReadInt32();
        DataTypeHash = rd.ReadUInt16();
        uint dataLength = rd.ReadUInt32();

        RawData = new byte[dataLength];
        for (int i = 0; i < dataLength; i++) 
            RawData[i] = rd.ReadByte();
    }

    public override void WriteTo(BinaryWriter wr)
    {
        wr.Write("CRRL".ToCharArray());
        wr.Write((byte)Target);
        wr.Write(TargetId);
        wr.Write(DataTypeHash);

        using MemoryStream ms = new MemoryStream();
        using BinaryWriter msWriter = new BinaryWriter(ms);
        
        WriteData(msWriter);

        RawData = ms.ToArray();
        
        wr.Write((uint)RawData.Length);
        wr.Write(RawData);
    }

    public void DeserializeData()
    {
        using MemoryStream ms = new MemoryStream(RawData);
        using BinaryReader msReader = new BinaryReader(ms);
        
        ReadData(msReader);
    }

    public void SerializeData()
    {
        using MemoryStream ms = new MemoryStream();
        using BinaryWriter msWriter = new BinaryWriter(ms);
        
        WriteData(msWriter);

        RawData = ms.ToArray();
    }

    protected virtual void ReadData(BinaryReader rd) {}
    protected virtual void WriteData(BinaryWriter wr) {}

    public RelayPacket Deserialize(byte[] data)
    {
        using MemoryStream ms = new MemoryStream(data);
        using BinaryReader msReader = new BinaryReader(ms);
        
        ReadFrom(msReader);
        return this;
    }

    public RelayPacket<T> Unpack<T>() where T : SerializablePacket, new()
    {
        RelayPacket<T> target = (RelayPacket<T>)this;
        target.DeserializeData();

        return target;
    }
}

public class RelayPacket<T> : RelayPacket
    where T : SerializablePacket, new()
{
    public T Data { get; set; }

    public RelayPacket(PacketTarget target, T data, int targetId = 0)
    {
        Target = target;
        TargetId = targetId;
        DataTypeHash = typeof(T).GetTypeHash();
        Data = data;
    }
    
    protected override void ReadData(BinaryReader rd)
    {
        if (IsInvalid) return;

        using MemoryStream ms = new MemoryStream(RawData);
        using BinaryReader msReader = new BinaryReader(ms);
        
        Data = new T();
        Data.ReadFrom(msReader);
    }

    protected override void WriteData(BinaryWriter wr)
    {
        if (IsInvalid) return;
        
        using MemoryStream ms = new MemoryStream();
        using BinaryWriter msWriter = new BinaryWriter(ms);
        Data.WriteTo(msWriter);
        RawData = ms.ToArray();
        
        wr.Write((uint) RawData.Length);
        wr.Write(RawData);
    }

    public RelayPacket Pack()
    {
        SerializeData();

        return this;
    }
}

public enum PacketTarget
{
    None = 0,
    Relay = 1,
    Host = 2,
    Client = 3
}