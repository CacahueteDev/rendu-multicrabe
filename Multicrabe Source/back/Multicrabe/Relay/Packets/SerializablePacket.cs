﻿using System.Reflection;

namespace Multicrabe.Relay.Packets;

public abstract class SerializablePacket
{
    static Dictionary<Type, ushort> computedHashs = new();

    public abstract void ReadFrom(BinaryReader rd);
    public abstract void WriteTo(BinaryWriter wr);

    public static SerializablePacket? RetrieveFromHash(ushort hash)
    {
        foreach (Type t in Assembly.GetExecutingAssembly().GetTypes()
                     .Where(type => type.IsSubclassOf(typeof(SerializablePacket))))
        {
            ushort typeHash = computedHashs.ContainsKey(t) ? computedHashs[t] : t.GetTypeHash();
            if (!computedHashs.ContainsKey(t)) computedHashs.Add(t, typeHash);

            if (hash == typeHash) return (SerializablePacket)Activator.CreateInstance(t)!;
        }

        return null;
    }

    public ushort GetTypeHash() =>
        computedHashs.ContainsKey(GetType()) ? computedHashs[GetType()] : GetType().GetTypeHash();
}