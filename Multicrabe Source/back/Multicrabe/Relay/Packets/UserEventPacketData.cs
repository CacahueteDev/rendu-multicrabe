﻿namespace Multicrabe.Relay.Packets;

public class UserEventPacketData : SerializablePacket
{
    public int ConnectionId { get; private set; }
    public EventType Event { get; private set; }

    public UserEventPacketData()
    {
        
    }

    public UserEventPacketData(int connectionId, EventType eventType)
    {
        ConnectionId = connectionId;
        Event = eventType;
    }
    
    public override void ReadFrom(BinaryReader rd)
    {
        ConnectionId = rd.ReadInt32();
        Event = (EventType) rd.ReadByte();
    }

    public override void WriteTo(BinaryWriter wr)
    {
        wr.Write(ConnectionId);
        wr.Write((byte)Event);
    }

    public enum EventType
    {
        None,
        Connected,
        Disconnected
    }
}