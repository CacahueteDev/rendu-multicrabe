﻿namespace Multicrabe.Relay.Packets;

public class DisconnectUserPacketData : SerializablePacket
{
    public int ConnectionId { get; private set; }

    public DisconnectUserPacketData()
    {
        
    }

    public DisconnectUserPacketData(int connectionId)
    {
        ConnectionId = connectionId;
    }
    
    public override void ReadFrom(BinaryReader rd)
    {
        ConnectionId = rd.ReadInt32();
    }

    public override void WriteTo(BinaryWriter wr)
    {
        wr.Write(ConnectionId);
    }
}