﻿namespace Multicrabe.Relay.Packets;

public class TransferPacketData : SerializablePacket
{
    public byte[] Data { get; set; }

    public TransferPacketData()
    {
        
    }

    public TransferPacketData(byte[] data)
    {
        Data = data;
    }
    
    public override void ReadFrom(BinaryReader rd)
    {
        int length = rd.ReadInt32();
        Data = rd.ReadBytes(length);
    }

    public override void WriteTo(BinaryWriter wr)
    {
        wr.Write(Data.Length);
        wr.Write(Data);
    }
}