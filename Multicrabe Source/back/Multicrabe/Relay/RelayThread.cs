﻿using Multicrabe.Core;

namespace Multicrabe.Relay;

public static class RelayThread
{
    static Thread thread;
    static bool running = true;

    public static void Start()
    {
        thread = new Thread(ThreadMain);
        thread.Start();
    }

    public static void Stop()
    {
        running = false;
    }

    private static void ThreadMain()
    {
        while (running)
        {
            try
            {
                foreach (var list in MulticrabeManager.Relays.Values)
                {
                    foreach (GameRelay relay in list) relay.Tick();
                }
            }
            catch (Exception e)
            {
                
            }
        }
    }
}