﻿using kcp2k;
using Multicrabe.Relay.Packets;

namespace Multicrabe;

public static class Extensions
{
    public static void SendPacket(this KcpClient client, RelayPacket packet, KcpChannel channel = KcpChannel.Reliable)
    {
        using MemoryStream ms = new MemoryStream();
        using BinaryWriter wr = new BinaryWriter(ms);

        packet.WriteTo(wr);
        
        client.Send(ms.ToArray(), channel);
    }
}