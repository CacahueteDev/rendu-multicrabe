class GameList extends HTMLElement {
  constructor() {
    super();
    this.games = [];
  }

  connectedCallback() {
    this._fetchGames().then((games) => {
      this.games = games.data;
      this.render();
    });
  }

  async _fetchGames() {
    const response = await fetch("http://vps-82e1d4ee.vps.ovh.net/games");
    const data = await response.json();

    return data;
  }

  render() {
    const ul = document.createElement("ul");
    ul.classList.add("game-list");

    this.games.forEach((game) => {
      const li = document.createElement("button");

      li.classList.add("game-list-item", game);
      li.setAttribute("game", game);
      li.textContent = Capitalize(game);
      ul.appendChild(li);
    });

    this.innerHTML = "";
    this.appendChild(ul);
  }
}

customElements.define("game-list", GameList);

class ServerList extends HTMLElement {
  constructor() {
    super();
    this.serverList = [];
  }

  connectedCallback() {
    window.onload = () => {
      const games = document.querySelectorAll(".game-list-item");

      games.forEach((game) => {
        games[0].click();
        game.addEventListener("click", () => {
          this._fetchServerList(game.getAttribute("game")).then((servers) => {
            this.serverList = servers.data;
            this.render();
          });
        });
      });
    };
  }

  async _fetchServerList(gameName) {
    const response = await fetch(
      `http://vps-82e1d4ee.vps.ovh.net/serverlist/${gameName}`,
    );
    const data = await response.json();
    return data;
  }

  render() {
    this.innerHTML = "";
    const header = document.createElement("div");
    header.classList.add("sidebar-content-header");
    this.serverList.forEach((element) => {
      header.style.backgroundImage = `url(res/${element.gameName}.png)`;
      header.style.backgroundSize = "cover";
      header.style.backgroundRepeat = "no-repeat";
    });

    const title = document.createElement("span");
    title.classList.add("sidebar-content-title");
    title.textContent = Capitalize(this.serverList[0].gameName);
    header.appendChild(title);

    const subtitle = document.createElement("span");
    subtitle.classList.add("sidebar-content-subtitle");
    subtitle.textContent = `${this.serverList.length} serveurs en ligne`;
    header.appendChild(subtitle);

    const body = document.createElement("div");
    body.classList.add("sidebar-content-body");

    const serverWrapper = document.createElement("div");
    serverWrapper.classList.add("servers-wrapper");
    this.serverList.forEach((server) => {
      const serverDiv = document.createElement("div");
      serverDiv.classList.add("server");

      const serverInfo = document.createElement("div");
      serverInfo.classList.add("server-info");

      const serverNameAndPlayers = document.createElement("div");
      serverNameAndPlayers.classList.add("server-name-and-players");

      const serverGameVersionAndAddress = document.createElement("div");
      serverGameVersionAndAddress.classList.add(
        "server-game-version-and-adress",
      );

      const serverNameSpan = document.createElement("span");
      serverNameSpan.classList.add("server-name");
      serverNameSpan.textContent = server.name;
      serverNameAndPlayers.appendChild(serverNameSpan);

      const serverPlayersSpan = document.createElement("span");
      serverPlayersSpan.classList.add("server-players");
      serverPlayersSpan.textContent =
        `${server.players.current}/${server.players.max}`;
      serverNameAndPlayers.appendChild(serverPlayersSpan);

      serverInfo.appendChild(serverNameAndPlayers);
      serverInfo.appendChild(serverGameVersionAndAddress);

      const serverGameVersionSpan = document.createElement("span");
      serverGameVersionSpan.classList.add("server-game-version");
      serverGameVersionSpan.textContent = `${
        Capitalize(server.gameName)
      } : ${server.gameVersion}`;

      const serverAddressSpan = document.createElement("span");
      serverAddressSpan.classList.add("server-address");
      serverAddressSpan.textContent = server.address;

      serverGameVersionAndAddress.appendChild(serverAddressSpan);
      serverGameVersionAndAddress.appendChild(serverGameVersionSpan);

      serverDiv.appendChild(serverInfo);
      serverWrapper.appendChild(serverDiv);
      body.appendChild(serverWrapper);

      this.appendChild(header);
      this.appendChild(body);
    });
  }
}

customElements.define("server-list", ServerList);

const Capitalize = (str) => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};
